﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using Moq;
using NUnit.Framework;
using ProSkive.Services.Notifications.Controllers.V1;
using ProSkive.Services.Notifications.Mapping;
using ProSkive.Services.Notifications.Models;
using ProSkive.Services.Notifications.Repositories;
using ProSkive.Services.Notifications.Resources.V1;
using ProSkive.Lib.TestsCommon;
using ProSkive.Tests.Services.Notifications.Repositories;

namespace ProSkive.Tests.Services.Notifications.UnitTests.ControllerTests.V1
{
    [TestFixture]
    [Category("Unit"), Category("Controller"), Category("NotificationsController")]
    public class NotificationsControllerTests
    {
        private Mock<ILogger<NotificationsController>> mockLogger;
        private IMapper mapper;
        
        [SetUp]
        public void SetUp()
        {
            mockLogger = new Mock<ILogger<NotificationsController>>();
            mapper = TestMapperFactory.GetMapper<MappingProfile>();
        }
        
        /* *************
         * Index
         * *************/

        [Test]
        [TestCase(null)]
        [TestCase(false)]
        [TestCase(true)]
        public async Task Index_WhenCalled_RespondeWith200Ok(bool? dismissed)
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.GetNotificationsAsync(dismissed))
                .Returns(MockNotificationRepository.GetNotificationsAsync(dismissed));

            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            var expectedNotifications = mapper.Map<List<Notification>, List<ViewNotificationResource>>(await MockNotificationRepository.GetNotificationsAsync(dismissed));

            // Act
            var result = await controller.Index(dismissed);
            
            // Assert
            Assert.IsNull(result.Result);
            Assert.IsNotNull(result.Value);

            var notifications = result.Value;
            notifications.Should().BeEquivalentTo(expectedNotifications);
        }
        
        /* *************
         * FindById
         * *************/

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public async Task FindById_InvalidId_RespondWuth400BadRequest(string id)
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            
            // Act
            var result = await controller.FindById(id);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<BadRequestObjectResult>());
        }

        [Test]
        public async Task FindById_NoResourceWithId_RespondWith404NotFound()
        {
            // Arrange
            const string id = "-123";
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.FindNotificationByIdAsync(id))
                .Returns(MockNotificationRepository.FindNotificationByIdAsync(id));

            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            
            // Act
            var result = await controller.FindById(id);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<NotFoundObjectResult>());
        }

        [Test]
        public async Task FindById_ResourceWithIdFound_RespondWith200Ok()
        {
            // Arrange
            const string id = "000000000000000000000000";
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.FindNotificationByIdAsync(id))
                .Returns(MockNotificationRepository.FindNotificationByIdAsync(id));

            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            var expectedNotification = mapper.Map<Notification, ViewNotificationResource>(await MockNotificationRepository.FindNotificationByIdAsync(id));
            
            // Act
            var result = await controller.FindById(id);
            
            // Assert          
            Assert.IsNull(result.Result);
            Assert.IsNotNull(result.Value);

            var notification = result.Value;
            notification.Should().BeEquivalentTo(expectedNotification);
        }
        
        /* *************
         * FindByTag
         * *************/

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public async Task FindByTag_InvalidTag_RespondWith400BadRequest(string tag)
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper); 
            
            // Act
            var result = await controller.FindByTag(tag);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<BadRequestObjectResult>());
        }

        [Test]
        [TestCase("project-service", null)]
        [TestCase("no-service", null)]
        [TestCase("project-service", true)]
        [TestCase("project-service", false)]
        public async Task FindByTag_ValidTag_RespondWith200Ok(string tag, bool? dismissed)
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.FindNotificationsByTagAsync(tag, dismissed))
                .Returns(MockNotificationRepository.FindNotificationsByTagAsync(tag, dismissed));

            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            var expectedNotifications = mapper.Map<List<Notification>, List<ViewNotificationResource>>(
                    await MockNotificationRepository.FindNotificationsByTagAsync(tag, dismissed)
                );
            
            // Act
            var result = await controller.FindByTag(tag, dismissed);
            
            // Assert
            Assert.IsNull(result.Result);
            Assert.IsNotNull(result.Value);

            var notifications = result.Value;
            notifications.Should().BeEquivalentTo(expectedNotifications);
        }

        /* *************
         * FindByListener
         * *************/

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public async Task FindByListener_InvalidListener_RespondWith400BadRequest(string listener)
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            
            // Act
            var result = await controller.FindByListener(listener);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<BadRequestObjectResult>());
        }
        
        [Test]
        [TestCase("123-456", null)]
        [TestCase("000-000", null)]
        [TestCase("123-456", true)]
        [TestCase("123-456", false)]
        public async Task FindByListener_ValidListener_RespondWith200Ok(string listener, bool? dismissed)
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.FindNotificationsByListenerAsync(listener, dismissed))
                .Returns(MockNotificationRepository.FindNotificationsByListenerAsync(listener, dismissed));

            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            var expectedNotifications = mapper.Map<List<Notification>, List<ViewNotificationResource>>(
                    await MockNotificationRepository.FindNotificationsByListenerAsync(listener, dismissed)
                );
            
            // Act
            var result = await controller.FindByListener(listener, dismissed);
            
            // Assert
            //Assert.That(result, Is.TypeOf<OkObjectResult>());

            //var okResult = (OkObjectResult) result.Result;
            //Assert.That(okResult.Value, Is.TypeOf<List<ViewNotificationResource>>());
            
            Assert.IsNull(result.Result);
            Assert.IsNotNull(result.Value);

            var notifications = result.Value;
            notifications.Should().BeEquivalentTo(expectedNotifications);
        }
        
        /* *************
         * FindByTagAndListener
         * *************/

        [Test]
        [TestCase(null, null)]
        [TestCase("", null)]
        [TestCase(" ", null)]
        [TestCase(null, "")]
        [TestCase(null, " ")]
        [TestCase("", "")]
        [TestCase(" ", " ")]
        public async Task FindByTagAndListener_InvalidTagOrListener_RespondWith400BadRequest(string tag, string listener)
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            
            // Act
            var result = await controller.FindByTagAndListener(tag, listener);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<BadRequestObjectResult>());
        }

        [Test]
        [TestCase("project-service", "123-456", null)]
        [TestCase("project-service", "123-456", true)]
        [TestCase("project-service", "123-456", false)]
        [TestCase("project-service", "000-000", null)]
        [TestCase("project-service", "000-000", true)]
        [TestCase("project-service", "000-000", false)]
        [TestCase("no-service", "000-000", null)]
        [TestCase("no-service", "000-000", true)]
        [TestCase("no-service", "000-000", false)]
        [TestCase("no-service", "123-456", null)]
        [TestCase("no-service", "123-456", true)]
        [TestCase("no-service", "123-456", false)]
        public async Task FindByTagAndListener_ValidTagAndListener_RespondWith200Ok(string tag, string listener, bool? dismissed)
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.FindNotificationsByTagAndListenerAsync(tag, listener, dismissed))
                .Returns(MockNotificationRepository.FindNotificationsByTagAndListenerAsync(tag, listener, dismissed));

            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            var expectedNotifications = mapper.Map<List<Notification>, List<ViewNotificationResource>>(
                    await MockNotificationRepository.FindNotificationsByTagAndListenerAsync(tag, listener, dismissed)
                );
            
            // Act
            var result = await controller.FindByTagAndListener(tag, listener, dismissed);
            
            // Assert
            Assert.IsNull(result.Result);
            Assert.IsNotNull(result.Value);

            var notifications = result.Value;
            notifications.Should().BeEquivalentTo(expectedNotifications);
        }
        
        /* *************
         * Create
         * *************/

        [Test]
        public async Task Create_InvalidInput_RespondWith400BadRequest()
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            controller.ModelState.AddModelError("", "Error");
            
            // Act
            var result = await controller.Create(null);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<BadRequestObjectResult>());
        }

        [Test]
        public async Task Create_ValidInput_RespondWith201Created()
        {
            // Arrange
            var createNotificationResource = new CreateNotificationResource()
            {
                Listener = "123-456",
                Message = "Ein neues Projekt wurde angelegt",
                Link = "http://project-service.fake/10",
                Tag = "project-service"
            };

            var notification = mapper.Map<CreateNotificationResource, Notification>(createNotificationResource);
            
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.SaveNotificationAsync(It.IsAny<Notification>()))
                .Returns(MockNotificationRepository.SaveNotificationAsync(notification));
            
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            
            // Act
            var result = await controller.Create(createNotificationResource);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<CreatedAtRouteResult>());

            var createdResult = (CreatedAtRouteResult) result.Result;
            Assert.That(createdResult.Value, Is.TypeOf<ViewNotificationResource>());

            var viewNotificationResource = (ViewNotificationResource) createdResult.Value;
            notification.Date = viewNotificationResource.Date;
            notification.Dismissed = false;

            var expectedViewNotificationResource = mapper.Map<Notification, ViewNotificationResource>(notification);
            viewNotificationResource.Should().BeEquivalentTo(expectedViewNotificationResource);
        }

        /* *************
         * Create (Multiple)
         * *************/

        [Test]
        public async Task CreateMultiple_InvalidInput_RespondWith400BadRequest()
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            controller.ModelState.AddModelError("", "Error");
            
            // Act
            var result = await controller.CreateMultiple(null);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<BadRequestObjectResult>());
        }

        [Test]
        public async Task CreateMultiple_ValidInput_RespondWith200Ok()
        {
            // Arrange
            var createMultipleNotificationsResource = new CreateMultipleNotificationsResource()
            {
                Listeners = new List<string>()
                {
                    "listener_1",
                    "listener_2"
                },
                Message = "A notification was created for two listeners",
                Link = "http://fake.fake",
                Tag = "fake"
            };
            
            // Expected Results
            var expectedCreatedNotificationsList = new List<ViewNotificationResource>();
            Notification notification1 = new Notification()
            {
                Id = new ObjectId(),
                Listener = createMultipleNotificationsResource.Listeners[0],
                Message = createMultipleNotificationsResource.Message,
                Link = createMultipleNotificationsResource.Link,
                Tag = createMultipleNotificationsResource.Tag,
                Dismissed = false,
                Date = DateTime.Now,
            };
            
            Notification notification2 = new Notification()
            {
                Id = new ObjectId(),
                Listener = createMultipleNotificationsResource.Listeners[1],
                Message = createMultipleNotificationsResource.Message,
                Link = createMultipleNotificationsResource.Link,
                Tag = createMultipleNotificationsResource.Tag,
                Dismissed = false,
                Date = DateTime.Now,
            };
            
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.SaveNotificationAsync(It.IsAny<Notification>()))
                .Returns((Notification n) => Task.FromResult(n));
            
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            
            // Act
            var result = await controller.CreateMultiple(createMultipleNotificationsResource);
            
            // Assert
            Assert.IsNull(result.Result);
            Assert.IsNotNull(result.Value);
            var viewNotificationResourceList = result.Value;
            
            notification1.Id = new ObjectId(viewNotificationResourceList[0].Id);
            notification1.Date = viewNotificationResourceList[0].Date;
            notification2.Id = new ObjectId(viewNotificationResourceList[1].Id);
            notification2.Date = viewNotificationResourceList[1].Date;
            
            expectedCreatedNotificationsList.Add(mapper.Map<Notification, ViewNotificationResource>(notification1));
            expectedCreatedNotificationsList.Add(mapper.Map<Notification, ViewNotificationResource>(notification2));
            
            viewNotificationResourceList.Should().BeEquivalentTo(expectedCreatedNotificationsList);
        }

        /* *************
         * Dismiss
         * *************/

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public async Task Dismiss_InvalidId_RespondWith400BadRequest(string id)
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            
            // Act
            var result = await controller.Dismiss(id);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<BadRequestObjectResult>());
        }

        [Test]
        public async Task Dismiss_ResourceNotAvailable_RespondWith404NotFound()
        {
            // Arrange
            const string id = "000-000";
            var mockRepo = new Mock<INotificationRepository>();
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            
            // Act
            var result = await controller.Dismiss(id);
            
            // Assert
            Assert.That(result.Result, Is.TypeOf<NotFoundObjectResult>());
        }

        [Test]
        public async Task Dismiss_ValidIdAndResourceFound_RespondWith200Ok()
        {
            // Arrange
            const string id = "000000000000000000000000";
            
            var notification = await MockNotificationRepository.FindNotificationByIdAsync(id);
            notification.Dismissed = true;
            var expectedViewNotificationResource = mapper.Map<Notification, ViewNotificationResource>(notification);
                
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.DismissNotificationAsync(It.IsAny<Notification>(), false))
                .Returns(MockNotificationRepository.DismissNotificationAsync(notification));
            mockRepo.Setup(repo => repo.FindNotificationByIdAsync(It.IsAny<string>()))
                .Returns(MockNotificationRepository.FindNotificationByIdAsync(id));
            
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
            
            // Act
            var result = await controller.Dismiss(id);
            
            // Assert
            Assert.IsNull(result.Result);
            Assert.IsNotNull(result.Value);

            var viewNotificationResource = result.Value;
            viewNotificationResource.Should().BeEquivalentTo(expectedViewNotificationResource);
        }
        
        /* *************
        * UndoDismiss
        * *************/

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public async Task UndoDismiss_InvalidId_RespondWith400BadRequest(string id)
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
           
            // Act
            var result = await controller.UndoDismiss(id);
           
            // Assert
            Assert.That(result.Result, Is.TypeOf<BadRequestObjectResult>());
        }

        [Test]
        public async Task UndoDismiss_ResourceNotAvailable_RespondWith404NotFound()
        {
            // Arrange
            const string id = "000-000";
            var mockRepo = new Mock<INotificationRepository>();
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
           
            // Act
            var result = await controller.UndoDismiss(id);
           
            // Assert
            Assert.That(result.Result, Is.TypeOf<NotFoundObjectResult>());
        }

        [Test]
        public async Task UndoDismiss_ValidIdAndResourceFound_RespondWith200Ok()
        {
            // Arrange
            const string id = "000000000000000000000000";
           
            var notification = await MockNotificationRepository.FindNotificationByIdAsync(id);
            notification.Dismissed = false;
            var expectedViewNotificationResource = mapper.Map<Notification, ViewNotificationResource>(notification);
               
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.DismissNotificationAsync(It.IsAny<Notification>(), true))
                .Returns(MockNotificationRepository.DismissNotificationAsync(notification, true));
            mockRepo.Setup(repo => repo.FindNotificationByIdAsync(It.IsAny<string>()))
                .Returns(MockNotificationRepository.FindNotificationByIdAsync(id));
           
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
           
            // Act
            var result = await controller.UndoDismiss(id);
           
            // Assert
            Assert.IsNull(result.Result);
            Assert.IsNotNull(result.Value);

            var viewNotificationResource = result.Value;
            viewNotificationResource.Should().BeEquivalentTo(expectedViewNotificationResource);
        }
       
        /* *************
        * Delete
        * *************/

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public async Task Delete_InvalidId_RespondWithBadRequest(string id)
        {
            // Arrange
            var mockRepo = new Mock<INotificationRepository>();
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
           
            // Act
            var result = await controller.Delete(id);
           
            // Assert
            Assert.That(result, Is.TypeOf<BadRequestObjectResult>());
        }

        [Test]
        public async Task Delete_ResourceNotFound_RespondWith404NotFound()
        {
            // Arrange
            const string id = "-00000000";
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.DeleteNotification(It.IsAny<string>()))
                .Returns(MockNotificationRepository.DeleteNotification(id));
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
           
            // Act
            var result = await controller.Delete(id);
           
            // Assert
            Assert.That(result, Is.TypeOf<NotFoundObjectResult>());
        }

        [Test]
        public async Task Delete_ValidIdAndResourceFound_RespondWith204NoContent()
        {
            // Arrange
            const string id = "000000000000000000000000";
            var mockRepo = new Mock<INotificationRepository>();
            mockRepo.Setup(repo => repo.DeleteNotification(It.IsAny<string>()))
                .Returns(MockNotificationRepository.DeleteNotification(id));
            var controller = new NotificationsController(mockLogger.Object, mockRepo.Object, mapper);
           
            // Act
            var result = await controller.Delete(id);
           
            // Assert
            Assert.That(result, Is.TypeOf<NoContentResult>());
        }
    }
}