﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using ProSkive.Services.Notifications.Controllers.V1;
using ProSkive.Services.Notifications.Mapping;
using ProSkive.Services.Notifications.Models;
using ProSkive.Services.Notifications.Resources.V1;
using ProSkive.Services.Notifications.Services;
using ProSkive.Lib.TestsCommon;
using ProSkive.Tests.Services.Notifications.Services;

namespace ProSkive.Tests.Services.Notifications.UnitTests.ControllerTests.V1
{
    [TestFixture]
    [Category("Unit"), Category("Controller"), Category("MailController")]
    public class MailControllerTests
    {
        private Mock<ILogger<MailController>> mockLogger;
        private IMapper mapper;
        
        [SetUp]
        public void SetUp()
        {
            mockLogger = new Mock<ILogger<MailController>>();
            mapper = TestMapperFactory.GetMapper<MappingProfile>();
        }

        [Test]
        public void SendMail_InvalidModel_RespondWith400BadRequest()
        {
            // Arrange
            var sendMailResource = new SendMailResource();
            var controller = new MailController(mockLogger.Object, null, mapper);
            controller.ModelState.AddModelError("", "Error");
            
            // Act
            var result = controller.SendMail(sendMailResource);

            // Assert
            Assert.That(result, Is.TypeOf<BadRequestObjectResult>());
        }

        [Test]
        public void SendMail_MailNotProcessable_RespondWith500InternalServerError()
        {
            // Arrange
            var sendMailResource = new SendMailResource()
            {
                From = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                To = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Woman",
                        Address = "test@woman.fake"
                    }
                },
                Cc = null,
                Bcc = null,
                Subject = "This is a test mail",
                Body = "Can you check if the mail arrives?"
            };

            var mail = mapper.Map<SendMailResource, Mail>(sendMailResource);
            
            // Mock
            var mockService = new Mock<IMailService>();
            mockService.Setup(service => service.SendMail(It.IsAny<Mail>())).Returns(MockMailService.SendMail(mail, pass: false));
            
            var controller = new MailController(mockLogger.Object, mockService.Object, mapper);

            // Act
            var result = controller.SendMail(sendMailResource);

            // Assert
            Assert.That(result, Is.InstanceOf<ObjectResult>());

            var internalServerErrorResult = (ObjectResult) result;
            Assert.That(internalServerErrorResult.StatusCode, Is.EqualTo(StatusCodes.Status500InternalServerError));
        }

        [Test]
        public void SendMail_ValidAndProcessableMail_RespondWith202Accepted()
        {
            // Arrange
            var sendMailResource = new SendMailResource()
            {
                From = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                To = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Woman",
                        Address = "test@woman.fake"
                    }
                },
                Cc = null,
                Bcc = null,
                Subject = "This is a test mail",
                Body = "Can you check if the mail arrives?"
            };

            var mail = mapper.Map<SendMailResource, Mail>(sendMailResource);
            
            // Mock
            var mockService = new Mock<IMailService>();
            mockService.Setup(service => service.SendMail(It.IsAny<Mail>())).Returns(MockMailService.SendMail(mail));
            
            var controller = new MailController(mockLogger.Object, mockService.Object, mapper);

            // Act
            var result = controller.SendMail(sendMailResource);

            // Assert
            Assert.That(result, Is.TypeOf<AcceptedResult>());
        }
    }
}