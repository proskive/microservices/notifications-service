﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using NUnit.Framework;
using ProSkive.Services.Notifications.Resources.V1;
using ProSkive.Tests.Services.Notifications.Data;

namespace ProSkive.Tests.Services.Notifications.UnitTests.ValidationTests
{
    [TestFixture]
    [Category("Unit"), Category("Validation"), Category("MailValidation")]
    public class MailValidationTests
    {
        [Test]
        [TestCaseSource(typeof(MailTestData), nameof(MailTestData.SendMailResource_ShouldNotValidate))]
        public void Validate_SendMailResource_NotPassing(SendMailResource sendMailResource)
        {
            // Log
            Console.WriteLine($"Testing Object (Hash): {sendMailResource.GetHashCode()}");
            
            // Arrange
            var context = new ValidationContext(sendMailResource);
            var result = new List<ValidationResult>();
            
            // Act
            var valid = Validator.TryValidateObject(sendMailResource, context, result, true);
            
            // Assert
            Assert.That(valid, Is.False);
        }

        [Test]
        [TestCaseSource(typeof(MailTestData), nameof(MailTestData.SendMailResource_ShouldValidate))]
        public void Validate_SendMailResource_Passing(SendMailResource sendMailResource)
        {
            // Log
            Console.WriteLine($"Testing Object (Hash): {sendMailResource.GetHashCode()}");
            
            // Arrange
            var context = new ValidationContext(sendMailResource);
            var result = new List<ValidationResult>();
            
            // Act
            var valid = Validator.TryValidateObject(sendMailResource, context, result, true);
            
            // Assert
            Assert.That(valid, Is.True);
        }
    }
}