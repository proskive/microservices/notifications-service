﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using NUnit.Framework;
using ProSkive.Services.Notifications.Resources.V1;
using ProSkive.Tests.Services.Notifications.Data;

namespace ProSkive.Tests.Services.Notifications.UnitTests.ValidationTests
{
    [TestFixture]
    [Category("Unit"), Category("Validation"), Category("NotificationValidation")]
    public class NotificationValidationTests
    {

        [Test]
        [TestCaseSource(typeof(NotificationTestData), nameof(NotificationTestData.CreateNotificationResource_ShouldNotValidate))]
        public void Validate_CreateNotificationResource_NotPassing(CreateNotificationResource createNotificationResource)
        {
            // Log
            Console.WriteLine($"Testing Object (Hash): {createNotificationResource.GetHashCode()}");
            
            // Arrange
            var context = new ValidationContext(createNotificationResource, null, null);
            var result = new List<ValidationResult>();
            
            // Act
            var valid = Validator.TryValidateObject(createNotificationResource, context, result);
            
            // Assert
            Assert.That(valid, Is.False);
        }

        [Test]
        [TestCaseSource(typeof(NotificationTestData), nameof(NotificationTestData.CreateNotificationResource_ShouldValidate))]
        public void Validate_CreateNotificationResource_Passing(CreateNotificationResource createNotificationResource)
        {
            // Log
            Console.WriteLine($"Testing Object (Hash): {createNotificationResource.GetHashCode()}");
            
            // Arrange
            var context = new ValidationContext(createNotificationResource, null, null);
            var result = new List<ValidationResult>();
            
            // Act
            var valid = Validator.TryValidateObject(createNotificationResource, context, result);
            
            // Assert
            Assert.That(valid, Is.True);
        }

        [Test]
        [TestCaseSource(typeof(NotificationTestData), nameof(NotificationTestData.CreateMultipleNotificationsResource_ShouldNotValidate))]
        public void Validate_CreateMultipleNotificationsResource_NotPassing(CreateMultipleNotificationsResource createMultipleNotificationsResource)
        {
            // Log
            Console.WriteLine($"Testing Object (Hash): {createMultipleNotificationsResource.GetHashCode()}");
            
            // Arrange
            var context = new ValidationContext(createMultipleNotificationsResource, null, null);
            var result = new List<ValidationResult>();
            
            // Act
            var valid = Validator.TryValidateObject(createMultipleNotificationsResource, context, result, true);
            
            // Assert
            Assert.That(valid, Is.False);
        }

        [Test]
        [TestCaseSource(typeof(NotificationTestData), nameof(NotificationTestData.CreateMultipleNotificationsResource_ShouldValidate))]
        public void Validate_CreateMultipleNotificationsResource_Passing(CreateMultipleNotificationsResource createMultipleNotificationsResource)
        {
            // Log
            Console.WriteLine($"Testing Object (Hash): {createMultipleNotificationsResource.GetHashCode()}");
            
            // Arrange
            var context = new ValidationContext(createMultipleNotificationsResource, null, null);
            var result = new List<ValidationResult>();
            
            // Act
            var valid = Validator.TryValidateObject(createMultipleNotificationsResource, context, result, true);
            
            // Assert
            Assert.That(valid, Is.True);
        }
    }
}