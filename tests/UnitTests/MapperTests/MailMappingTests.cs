﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FluentAssertions;
using NUnit.Framework;
using ProSkive.Services.Notifications.Mapping;
using ProSkive.Services.Notifications.Models;
using ProSkive.Services.Notifications.Resources.V1;
using ProSkive.Lib.TestsCommon;

namespace ProSkive.Tests.Services.Notifications.UnitTests.MapperTests
{
    [TestFixture]
    [Category("Unit"), Category("Mapping"), Category("MailMapping")]
    public class MailMappingTests
    {
        private IMapper mapper;

        [SetUp]
        public void SetUp()
        {
            mapper = TestMapperFactory.GetMapper<MappingProfile>();
        }
        
        [Test]
        public void Map_SendMailResource_To_Mail()
        {
            // Arrange
            var sendMailResource = new SendMailResource()
            {
                From = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                To = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Woman",
                        Address = "test@woman.fake"
                    }
                },
                Cc = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Antother Man",
                        Address = "another@man.fake"
                    }
                },
                Bcc = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Antother Woman",
                        Address = "another@woman.fake"
                    }
                },
                Subject = "Passing validation mail",
                Body = "This mail should pass"
            };

            var expectedMail = new Mail()
            {
                From = new List<MailAddress>()
                {
                    new MailAddress()
                    {
                        Name = sendMailResource.From.FirstOrDefault().Name,
                        Address = sendMailResource.From.FirstOrDefault().Address
                    }
                },
                To = new List<MailAddress>()
                {
                    new MailAddress()
                    {
                        Name = sendMailResource.To.FirstOrDefault().Name,
                        Address = sendMailResource.To.FirstOrDefault().Address
                    }
                },
                Cc = new List<MailAddress>()
                {
                    new MailAddress()
                    {
                        Name = sendMailResource.Cc.FirstOrDefault().Name,
                        Address = sendMailResource.Cc.FirstOrDefault().Address
                    }
                },
                Bcc = new List<MailAddress>()
                {
                    new MailAddress()
                    {
                        Name = sendMailResource.Bcc.FirstOrDefault().Name,
                        Address = sendMailResource.Bcc.FirstOrDefault().Address
                    }    
                },
                Subject = sendMailResource.Subject,
                Body = sendMailResource.Body
            };

            // Act
            var mail = mapper.Map<SendMailResource, Mail>(sendMailResource);
            
            // Asert
            mail.Should().BeEquivalentTo(expectedMail);
        }

    }
}