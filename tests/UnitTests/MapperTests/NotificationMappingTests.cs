﻿using System;
using AutoMapper;
using FluentAssertions;
using MongoDB.Bson;
using NUnit.Framework;
using ProSkive.Services.Notifications.Mapping;
using ProSkive.Services.Notifications.Models;
using ProSkive.Services.Notifications.Resources.V1;
using ProSkive.Lib.TestsCommon;

namespace ProSkive.Tests.Services.Notifications.UnitTests.MapperTests
{
    [TestFixture]
    [Category("Unit"), Category("Mapping"), Category("NotificationMapping")]
    public class NotificationMappingTests
    {
        private IMapper mapper;
        
        [SetUp]
        public void SetUp()
        {
            mapper = TestMapperFactory.GetMapper<MappingProfile>();
        }

        [Test]
        public void Map_Notification_To_ViewNotificationResource()
        {
            // Arrange
            var notification = new Notification()
            {
                Id = new ObjectId(),
                Listener = "123-456",
                Message = "Ein neues Projekt wurde angelegt",
                Link = "http://project-service.fake/10",
                Dismissed = false,
                Date = DateTime.Now,
                Tag = "project-service"
            };

            var expectedViewNotificationResource = new ViewNotificationResource()
            {
                Id = notification.Id.ToString(),
                Listener = notification.Listener,
                Message = notification.Message,
                Link = notification.Link,
                Dismissed = notification.Dismissed,
                Date = notification.Date,
                Tag = notification.Tag
            };

            // Act
            var viewNotificationResource = mapper.Map<Notification, ViewNotificationResource>(notification);
            
            // Asert
            viewNotificationResource.Should().BeEquivalentTo(expectedViewNotificationResource);
        }

        [Test]
        public void Map_CreateNotificationResource_To_Notification()
        {
            // Arrange
            var createNotificationResource = new CreateNotificationResource()
            {
                Listener = "123-456",
                Message = "Ein neues Projekt wurde angelegt",
                Link = "http://project-service.fake/10",
                Tag = "project-service"
            };
            
            var expectedNotification = new Notification()
            {
                Id = new ObjectId(),
                Listener = createNotificationResource.Listener,
                Message = createNotificationResource.Message,
                Link = createNotificationResource.Link,
                Dismissed = false,
                Date = DateTime.Now,
                Tag = createNotificationResource.Tag
            };

            // Act
            var notification = mapper.Map<CreateNotificationResource, Notification>(createNotificationResource);
            notification.Date = expectedNotification.Date;
            
            // Asert
            notification.Should().BeEquivalentTo(expectedNotification);
        }
    }
}