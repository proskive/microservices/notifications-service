﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using Moq;
using ProSkive.Services.Notifications.Models;
using ProSkive.Services.Notifications.Repositories;
using ProSkive.Tests.Services.Notifications.Data;

namespace ProSkive.Tests.Services.Notifications.Repositories
{
    public static class MockNotificationRepository
    {
        public static Task<List<Notification>> GetNotificationsAsync(bool? dismissed = null)
        {
            if (dismissed.HasValue)
                return Task.FromResult(MockData.Notifications.Where(n => n.Dismissed == dismissed).ToList());

            return Task.FromResult(MockData.Notifications);
        }

        public static Task<Notification> FindNotificationByIdAsync(string id)
        {
            return Task.FromResult(MockData.Notifications.FirstOrDefault(n => n.Id.ToString().Equals(id)));
        }

        public static Task<List<Notification>> FindNotificationsByTagAsync(string tag, bool? dismissed = null)
        {
            if (dismissed.HasValue)
                return Task.FromResult(MockData.Notifications.Where(n => n.Dismissed == dismissed && n.Tag == tag)
                    .ToList());

            return Task.FromResult(MockData.Notifications.Where(n => n.Tag == tag).ToList());
        }

        public static Task<List<Notification>> FindNotificationsByListenerAsync(string listener, bool? dismissed = null)
        {
            if (dismissed.HasValue)
                return Task.FromResult(MockData.Notifications.Where(n => n.Dismissed == dismissed && n.Listener == listener)
                    .ToList());

            return Task.FromResult(MockData.Notifications.Where(n => n.Listener == listener).ToList());
        }

        public static Task<List<Notification>> FindNotificationsByTagAndListenerAsync(string tag, string listener, bool? dismissed = null)
        {
            if (dismissed.HasValue)
                return Task.FromResult(MockData.Notifications
                    .Where(n => n.Tag == tag && n.Listener == listener && n.Dismissed == dismissed).ToList());

            return Task.FromResult(MockData.Notifications.Where(n => n.Tag == tag && n.Listener == listener).ToList());
        }

        public static Task<Notification> SaveNotificationAsync(Notification notification)
        {
            
            return Task.FromResult(notification);
        }

        public static Task<Notification> DismissNotificationAsync(Notification notification, bool undo = false)
        {           
            notification.Dismissed = true ^ undo;
            return Task.FromResult(notification);
        }

        public static Task<bool> DeleteNotification(string id)
        {
            var notification = MockData.Notifications.FirstOrDefault(n => n.Id.ToString().Equals(id));
            if (notification is null)
                return Task.FromResult(false);

            return Task.FromResult(MockData.Notifications.Remove(notification));
        }
    }
}