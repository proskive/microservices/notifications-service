﻿using System.Collections.Generic;
using ProSkive.Services.Notifications.Resources.V1;

namespace ProSkive.Tests.Services.Notifications.Data
{
    public static class MailTestData
    {
        
        public static object[] SendMailResource_ShouldNotValidate = {
            new SendMailResource()
            {
                From = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                To = null,
                Cc = null,
                Bcc = null,
                Subject = "Not passing validation mail",
                Body = "This mail should not pass"
            }, 
            new SendMailResource()
            {
                From = null,
                To = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                Cc = null,
                Bcc = null,
                Subject = "Not passing validation mail",
                Body = "This mail should not pass"
            },
            new SendMailResource()
            {
                From = new List<MailAddressResource>(),
                To = new List<MailAddressResource>(),
                Cc = null,
                Bcc = null,
                Subject = "Not passing validation mail",
                Body = "This mail should not pass"
            },
            new SendMailResource()
            {
                From = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Woman",
                        Address = "test@woman.fake"
                    }
                },
                To = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                Cc = null,
                Bcc = null,
                Subject = null,
                Body = "This mail should not pass"
            },
            new SendMailResource()
            {
                From = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Woman",
                        Address = "test@woman.fake"
                    }
                },
                To = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                Cc = null,
                Bcc = null,
                Subject = "Not passing validation mail",
                Body = null
            },
            new SendMailResource()
            {
                From = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                To = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = null,
                        Address = "test@man.fake"
                    }
                },
                Cc = null,
                Bcc = null,
                Subject = "Not passing validation mail",
                Body = "This mail should not pass"
            },
            new SendMailResource()
            {
                From = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                To = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = null
                    }
                },
                Cc = null,
                Bcc = null,
                Subject = "Not passing validation mail",
                Body = "This mail should not pass"
            }
        };

        public static object[] SendMailResource_ShouldValidate =
        {
            new SendMailResource()
            {
                From = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                To = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Woman",
                        Address = "test@woman.fake"
                    }
                },
                Cc = null,
                Bcc = null,
                Subject = "Passing validation mail",
                Body = "This mail should pass"
            },
            new SendMailResource()
            {
                From = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                To = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Woman",
                        Address = "test@woman.fake"
                    }
                },
                Cc = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Antother Man",
                        Address = "another@man.fake"
                    }
                },
                Bcc = null,
                Subject = "Passing validation mail",
                Body = "This mail should pass"
            },
            new SendMailResource()
            {
                From = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                To = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Test Woman",
                        Address = "test@woman.fake"
                    }
                },
                Cc = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Antother Man",
                        Address = "another@man.fake"
                    }
                },
                Bcc = new List<MailAddressResource>()
                {
                    new MailAddressResource()
                    {
                        Name = "Antother Woman",
                        Address = "another@woman.fake"
                    }
                },
                Subject = "Passing validation mail",
                Body = "This mail should pass"
            }
        };

        /*
        
        public static IEnumerable<object[]> SendMailResource_ValidationPassingTestData()
        {
            return new List<object[]>()
            {
                new object[] { 
                    
                },
                new object[] { 
                    
                },
                new object[] { 
                    
                },
            };
        }*/
    }
}