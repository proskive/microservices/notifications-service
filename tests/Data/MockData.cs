﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using ProSkive.Services.Notifications.Models;

namespace ProSkive.Tests.Services.Notifications.Data
{
    public static class MockData
    {
        public static readonly List<Notification> Notifications = new List<Notification>()
        {
            new Notification()
            {
                Id = new ObjectId(),
                Listener = "123-456",
                Message = "Ein neues Projekt wurde angelegt",
                Link = "http://project-service.fake/2",
                Dismissed = true,
                Tag = "project-service",
                Date = new DateTime(2018, 1, 2)
            },
            new Notification()
            {
                Id = new ObjectId(),
                Listener = "123-456",
                Message = "Ein neues Projekt wurde angelegt",
                Link = "http://project-service.fake/1",
                Dismissed = true,
                Tag = "project-service",
                Date = new DateTime(2018, 1, 1)
            },
            new Notification()
            {
                Id = new ObjectId(),
                Listener = "987-654",
                Message = "Ein neuer Benutzer wurde angelegt",
                Link = "http://user-service.fake/3",
                Dismissed = true,
                Tag = "user-service",
                Date = new DateTime(2018, 1, 1)
            },
            new Notification()
            {
                Id = new ObjectId(),
                Listener = "123-456",
                Message = "Ihr Profil wurde aktualisiert",
                Link = "http://user-service.fake/2",
                Dismissed = false,
                Tag = "user-service",
                Date = new DateTime(2018, 1, 5)
            },
            new Notification()
            {
                Id = new ObjectId(),
                Listener = "123-456",
                Message = "Ein neues Projekt wurde angelegt",
                Link = "http://project-service.fake/2",
                Dismissed = false,
                Tag = "project-service",
                Date = new DateTime(2018, 1, 6)
            },
            new Notification()
            {
                Id = new ObjectId(),
                Listener = "987-654",
                Message = "Ein neuer Benutzer wurde angelegt",
                Link = "http://user-service.fake/4",
                Dismissed = false,
                Tag = "user-service",
                Date = new DateTime(2018, 1, 4)
            },
        };
    }
}