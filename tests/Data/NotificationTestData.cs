﻿using System.Collections.Generic;
using ProSkive.Services.Notifications.Resources.V1;

namespace ProSkive.Tests.Services.Notifications.Data
{
    public static class NotificationTestData
    {

        public static object[] CreateNotificationResource_ShouldNotValidate =
        {
            new CreateNotificationResource()
            {
                Listener = null,
                Message = "A new project has been created",
                Link = null,
                Tag = "project-service"
            },
            new CreateNotificationResource()
            {
                Listener = "123-456",
                Message = null,
                Link = null,
                Tag = "project-service"
            },
            new CreateNotificationResource()
            {
                Listener = "123-456",
                Message = "A new project has been created",
                Link = null,
                Tag = null
            }
        };
        
        public static object[] CreateNotificationResource_ShouldValidate =
        {
            new CreateNotificationResource()
            {
                Listener = "123-456",
                Message = "A new project has been created",
                Link = null,
                Tag = "project-service"
            },
            new CreateNotificationResource()
            {
                Listener = "123-456",
                Message = "A new project has been created",
                Link = "http://project-service/5",
                Tag = "project-service"
            }
        };

        public static object[] CreateMultipleNotificationsResource_ShouldNotValidate =
        {
            new CreateMultipleNotificationsResource()
            {
                Listeners = null,
                Message = "A new project has been created",
                Link = null,
                Tag = "project-service"
            },
            new CreateMultipleNotificationsResource()
            {
                Listeners = new List<string>(),
                Message = "A new project has been created",
                Link = null,
                Tag = "project-service"
            }
        };
        
        public static object[] CreateMultipleNotificationsResource_ShouldValidate =
        {
            new CreateMultipleNotificationsResource()
            {
                Listeners = new List<string>() { "123-456", "987-654" },
                Message = "A new project has been created",
                Link = null,
                Tag = "project-service"
            },
            new CreateMultipleNotificationsResource()
            {
                Listeners = new List<string>() { "123-456", "987-654" },
                Message = "A new project has been created",
                Link = "http://project-service/5",
                Tag = "project-service"
            }
        };
        
        // Notifications
        /*

        
        public static IEnumerable<object[]> CreateMultipleNotificationsResource_ValidationPassingTestData()
        {
            return new List<object[]>()
            {
                new object[] { 
                    
                },
                new object[] { 
                    
                }
            };
        }*/
    }
}