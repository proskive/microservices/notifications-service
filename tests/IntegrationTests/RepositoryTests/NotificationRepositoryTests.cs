﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using ProSkive.Clients.Notifications.V1;
using ProSkive.Clients.Notifications.V1.Resources;
using ProSkive.Lib.MongoDb.Helpers;
using ProSkive.Lib.MongoDb.Settings.Snapshots;
using ProSkive.Lib.TestsCommon;
using ProSkive.Lib.TestsCommon.Http;
using ProSkive.Services.Notifications.Models;

namespace ProSkive.Tests.Services.Notifications.IntegrationTests.RepositoryTests
{
    [TestFixture]
    [Category("Integration"), Category("Repositories"), Category("NotificationRepository")]
    public class NotificationRepositoryTests
    {
        private TestServer server;
        private NotificationsFlurlClient client;
        private MongoTestHelper<Notification> mongoHelper;

        [OneTimeSetUp]
        public void Init()
        {
            // Test Server
            server = TestServerHelper.CreateTestServer<TestStartup>(TestEnv.Get());
            

            // Flurl Factory Config
            FlurlTestHelper.UseInMemoryServer(server);
        }

        [OneTimeTearDown]
        public void Finish()
        {
            // Reset Flurl
            FlurlTestHelper.ResetClientFactory();
            
            // Dispose Server
            server.Dispose();
        }

        [SetUp]
        public void SetUp()
        {
            // Mongo
            if (server.Host.Services.GetService(typeof(IOptionsSnapshot<MongoOptionsSnapshot>)) is IOptionsSnapshot<MongoOptionsSnapshot> mongoSettings)
            {
                mongoHelper = new MongoTestHelper<Notification>(mongoSettings.Value.MongoDb);
                mongoHelper.CleanCollection();
            }
            
            // Flurl
            client = new NotificationsFlurlClient("http://localhost");
        }
        
        private async Task<ViewNotificationApiResource> AddDummyNotification(int i = 1)
        {
            return await client.CreateNotificationAsync(new CreateNotificationApiResource()
            {
                Listener = "123-456",
                Message = $"Neues Projekt {i} wurde angelegt",
                Link = $"http://project-service/{i}",
                Tag = "project-service"
            });
        }
        
        private async Task AddMultipleDummyNotifications()
        {
            // The following loop will result in 6 notifications:
            // - 4 Notifciations with Dismissed = false
            // - 2 Notifications with Dismissed = true
            for (var i = 1; i < 7; i++)
            {
                var addedNotification = await AddDummyNotification(i);
                if (i % 3 == 0)
                    await client.DismissNotificationAsync(addedNotification.Id);
            }
        }

        [Test]
        public async Task CreateNotification()
        {
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));

            var createNotificationResource = new CreateNotificationApiResource()
            {
                Listener = "123-456",
                Message = "Neues Projekt wurde angelegt",
                Link = "http://project-service/1",
                Tag = "project-service"
            };
            
            // Act
            var response = await client.CreateNotificationAsync(createNotificationResource);
            
            // Assert
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
        }
        
        [Test]
        public async Task CreateMultipleNotifications()
        {
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            
            var createMultipleNotificationsResource = new CreateMultipleNotificationApiResource()
            {
                Listeners = new List<string>()
                {
                    "Listener_1",
                    "Listener_2"
                },
                Message = "Created new Notifications",
                Link = "http://project-service/1",
                Tag = "project-service"
            };
            
            // Act
            var response = await client.CreateMultipleNotificationsAsync(createMultipleNotificationsResource);
            
            // Assert
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(2));
            Assert.That(response.Count, Is.EqualTo(2));
        }
        
        [Test]
        public async Task GetAllNotifications()
        {
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            await AddMultipleDummyNotifications();
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
            
            // Act
            var response = await client.GetAllNotificationsAsync();
            
            // Assert
            Assert.That(response.Count, Is.EqualTo(6));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
            
        }
        
        [Test]
        public async Task DismissNotification()
        {
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            
            // Add Notification
            var addedNotification = await AddDummyNotification();
            Assert.That(addedNotification.Dismissed, Is.False);
            
            // Act
            var dismissedNotification = await client.DismissNotificationAsync(addedNotification.Id);
            
            // Assert
            Assert.That(dismissedNotification.Dismissed, Is.True);
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
        }
        
        [Test]
        public async Task UndoDismissNotification()
        {
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            
            // Add Notification
            var addedNotification = await AddDummyNotification();
            var dismissedNotification = await client.DismissNotificationAsync(addedNotification.Id);
            Assert.That(dismissedNotification.Dismissed, Is.True);
            
            // Act
            var undoneDismissedNotification = await client.UndoDismissNotificationAsync(dismissedNotification.Id);
            
            // Assert
            Assert.That(undoneDismissedNotification.Dismissed, Is.False);
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
        }
        
        [Test]
        public async Task GetNotification()
        {
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            
            // Add Notification
            var addedNotification = await AddDummyNotification();
            
            // Act
            var foundNotification = await client.GetNotificationAsync(addedNotification.Id);
            
            // Assert
            foundNotification.Should().BeEquivalentTo(addedNotification, options => options.Excluding(n => n.Date));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
        }
        
        [Test]
        public async Task DeleteNotification()
        {
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            
            // Add Notification
            var addedNotification = await AddDummyNotification();
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(1));
            
            // Act
            var response = await client.DeleteNotificationWithResponseAsync(addedNotification.Id);
            
            // Assert
            Assert.That(HttpStatusCode.NoContent, Is.EqualTo(response.StatusCode));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
        }
        
        [Test]
        public async Task FindNotificationByTag_Dismissed_IsTrue()
        {
            const bool dismissed = true;
            
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            
            // Add Notification
            await AddMultipleDummyNotifications();
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
            
            // Act
            var foundNotifications = await client.GetAllNotificationsByTagAsync("project-service", dismissed);
            
            // Assert
            Assert.That(foundNotifications.Count, Is.EqualTo(2));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
        }
        
        [Test]
        public async Task FindNotificationByTag_Dismissed_IsFalse()
        {
            const bool dismissed = false;
            
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            
            // Add Notification
            await AddMultipleDummyNotifications();
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
            
            // Act
            var foundNotifications = await client.GetAllNotificationsByTagAsync("project-service", dismissed);
            
            // Assert
            Assert.That(foundNotifications.Count, Is.EqualTo(4));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
        }
        
        [Test]
        public async Task FindNotificationByListener_Dismissed_IsTrue()
        {
            const bool dismissed = true;
            
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            
            // Add Notification
            await AddMultipleDummyNotifications();
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
            
            // Act
            var foundNotifications = await client.GetAllNotificationsByListenerAsync("123-456", dismissed);
            
            // Assert
            Assert.That(foundNotifications.Count, Is.EqualTo(2));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
        }
        
        [Test]
        public async Task FindNotificationByListener_Dismissed_IsFalse()
        {
            const bool dismissed = false;
            
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            
            // Add Notification
            await AddMultipleDummyNotifications();
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
            
            // Act
            var foundNotifications = await client.GetAllNotificationsByListenerAsync("123-456", dismissed);
            
            // Assert
            Assert.That(foundNotifications.Count, Is.EqualTo(4));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
        }
        
        [Test]
        public async Task FindNotificationByTagAndListener_Dismissed_IsTrue()
        {
            const bool dismissed = true;
            
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            
            // Add Notification
            await AddMultipleDummyNotifications();
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
            
            // Act
            var foundNotifications = await client.GetAllNotificationsByListenerAndTagAsync("123-456", "project-service", dismissed);
            
            // Assert
            Assert.That(foundNotifications.Count, Is.EqualTo(2));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
        }
        
        [Test]
        public async Task FindNotificationByTagAndListener_Dismissed_IsFalse()
        {
            const bool dismissed = false;
            
            // Arrange
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(0));
            
            // Add Notification
            await AddMultipleDummyNotifications();
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
            
            // Act
            var foundNotifications = await client.GetAllNotificationsByListenerAndTagAsync("123-456", "project-service", dismissed);
            
            // Assert
            Assert.That(foundNotifications.Count, Is.EqualTo(4));
            Assert.That(mongoHelper.CountDocuments(), Is.EqualTo(6));
        }

    }
}