﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using ProSkive.Services.Notifications;
using ProSkive.Services.Notifications.Models;
using ProSkive.Services.Notifications.Services;
using ProSkive.Services.Notifications.Services.MailKit;

namespace ProSkive.Tests.Services.Notifications.IntegrationTests.ServiceTests
{
    [TestFixture]
    [Category("Integration"), Category("Services"), Category("MailService")]
    public class MailServiceTest
    {
        private Mock<ILogger<MailService>> mockLogger;
        private Mail testMail;

        [SetUp]
        public void SetUp()
        {
            // Logger
            mockLogger = new Mock<ILogger<MailService>>();
            
            // TestMail
            testMail = new Mail()
            {
                From = new List<MailAddress>()
                {
                    new MailAddress()
                    {
                        Name = "Test Man",
                        Address = "test@man.fake"
                    }
                },
                To = new List<MailAddress>()
                {
                    new MailAddress()
                    {
                        Name = "Test Woman",
                        Address = "test@woman.fake"
                    }
                },
                Cc = null,
                Bcc = null,
                Subject = "This is a test",
                Body = "<b>Test!</b>"
            };
        }

        private Mock<IOptionsSnapshot<AppSettings>> GetMockSettings(int port = 1025, bool isFake = false)
        {
            var actualPort = 0;

            if (isFake)
            {
                actualPort = port;
            }
            else
            {
                try
                {
                    actualPort = int.Parse(Environment.GetEnvironmentVariable("SMTP_PORT"));
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception {e.GetType().FullName} was thrown.");
                    actualPort = port;
                }
            }  
            
            var settings = new AppSettings()
            {
                Smtp = new SmtpSettings()
                {
                    Authentication = new UsernamePasswordAuth()
                    {
                        Enabled = false
                    },
                    Host = Environment.GetEnvironmentVariable("SMTP_HOST") ?? "localhost",
                    Port = actualPort
                }
            };
            
            var mockSettings = new Mock<IOptionsSnapshot<AppSettings>>();
            mockSettings.Setup(snapshot => snapshot.Value).Returns(settings);
            return mockSettings;
        }
        
        [Test]
        public void SendMail_NoRunningSmtpServer_ReturnFalse()
        {
            IMailService mailService = new MailService(mockLogger.Object, GetMockSettings(99999, true).Object);
            Assert.That(mailService.SendMail(testMail), Is.False);
        }

        [Test]
        public void SendMail_ValidRunningSmtpServer_ReturnTrue()
        {
            IMailService mailService = new MailService(mockLogger.Object, GetMockSettings().Object);
            Assert.That(mailService.SendMail(testMail), Is.True);
        }
    }
}