﻿using AutoMapper;
using LoggingAdvanced.Console;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProSkive.Lib.Common.Extensions;
using ProSkive.Lib.MongoDb.Contributors.Health;
using ProSkive.Lib.MongoDb.Settings.Snapshots;
using ProSkive.Services.Notifications;
using ProSkive.Services.Notifications.Repositories;
using ProSkive.Services.Notifications.Repositories.Mongo;
using ProSkive.Services.Notifications.Services;
using ProSkive.Services.Notifications.Services.MailKit;
using ProSkive.Lib.TestsCommon.Auth.FakeJwt;
using Steeltoe.Management.Endpoint.Health;

namespace ProSkive.Tests.Services.Notifications
{
    public class TestStartup
    {
        public TestStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Configs
            services.Configure<AppSettings>(Configuration);
            services.Configure<MongoOptionsSnapshot>(Configuration);
            services.Configure<ApiBehaviorOptions>(options => { options.SuppressModelStateInvalidFilter = true; });

            // Jwt -- CHANGED FOR TESTING
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = FakeJwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = FakeJwtBearerDefaults.AuthenticationScheme;
            })
            .AddFakeJwtBearer();
            
            // Eureka
            services.AddEureka(Configuration);
            
            // Health Contributors
            services.AddScoped<IHealthContributor, MongoHealthContributor>();
            
            // Health and Info Endpoints
            services.AddHealthAndInfoEndpoints(Configuration);
            
            // Dependency Injection
            services.AddAutoMapper();
            services.AddTransient<INotificationRepository, NotificationRepository>();
            services.AddTransient<IMailService, MailService>();
            
            // Lower Case
            services.AddRouting(options => options.LowercaseUrls = true);
                
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsoleAdvanced(Configuration.GetSection("Logging"));
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            // Auth
            app.UseAuthentication();

            // MVC Middleware
            app.UseMvc();
            
            // Use Eureka
            app.UseEureka(Configuration);
            
            // Management
            app.UseHealthAndInfoEndpoints();
        }
    }
}