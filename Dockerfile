# Builder
FROM microsoft/dotnet:2.1-sdk as notifications-builder

RUN mkdir /app
RUN mkdir /src
COPY ./src/* /src/
WORKDIR /src

RUN dotnet publish -c Release -o /app /property:PublishWithAspNetCoreTargetManifest=false

# Image
FROM microsoft/dotnet:2.1-aspnetcore-runtime

## Dependencies
RUN apt-get update
RUN apt-get install -y netcat-traditional
RUN apt-get install -y dos2unix

WORKDIR /app
COPY --from=notifications-builder /app .
COPY ./entrypoint.sh .
RUN chmod 755 ./entrypoint.sh
RUN dos2unix ./entrypoint.sh

ENV SELF_URL=http://localhost:9000
ENV AUTH_URL=http://localhost:8080/auth/realms/master
ENV EUREKA_URL=
ENV EUREKA_PORT=8761
ENV ASPNETCORE_ENVIRONMENT=Docker
ENV ASPNETCORE_URLS=http://+:9000
EXPOSE 9000
CMD ["./entrypoint.sh"]

## Healthcheck
HEALTHCHECK --start-period=60s --interval=60s --timeout=30s --retries=5 \
CMD nc -z localhost 9000
