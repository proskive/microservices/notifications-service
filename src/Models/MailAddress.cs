﻿namespace ProSkive.Services.Notifications.Models
{
    public class MailAddress
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}