﻿using System.Collections.Generic;
using ProSkive.Services.Notifications.Resources.V1;

namespace ProSkive.Services.Notifications.Models
{
    public class Mail
    {
        public List<MailAddress> From { get; set; }
        public List<MailAddress> To { get; set; }
        public List<MailAddress> Cc { get; set; }
        public List<MailAddress> Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}