﻿using System;
using MongoDB.Bson;

namespace ProSkive.Services.Notifications.Models
{
    public class Notification
    {
        public ObjectId Id { get; set; }
        public string Listener { get; set; }
        public string Message { get; set; }
        public string Link { get; set; }
        public bool Dismissed { get; set; }
        public string Tag { get; set; }
        public DateTime Date { get; set; }
    }
}