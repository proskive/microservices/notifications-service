﻿using System;
using System.Collections.Generic;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using ProSkive.Services.Notifications.Models;

namespace ProSkive.Services.Notifications.Services.MailKit
{
    public class MailService : IMailService
    {
        private readonly ILogger<MailService> logger;
        private readonly IOptionsSnapshot<AppSettings> settings;

        public MailService(ILogger<MailService> logger, IOptionsSnapshot<AppSettings> settings)
        {
            this.logger = logger;
            this.settings = settings;
        }
        
        public bool SendMail(Mail mail)
        {
            // Build mail
            var message = new MimeMessage();
            ConvertToInternetAddressList(message.From, mail.From);
            ConvertToInternetAddressList(message.To, mail.To);
            ConvertToInternetAddressList(message.Cc, mail.Cc);
            ConvertToInternetAddressList(message.Bcc, mail.Bcc);
            message.Subject = mail.Subject;
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = mail.Body
            };

            // Send Mail
            try
            {
                using (var client = new SmtpClient())
                {
                    var authEnabled = settings.Value.Smtp.Authentication.Enabled;
                    var username = settings.Value.Smtp.Authentication.Username;
                    var password = settings.Value.Smtp.Authentication.Password;
                    var host = settings.Value.Smtp.Host;
                    var port = settings.Value.Smtp.Port;
                
                    client.Connect(host, port);
                
                    if (authEnabled)
                        client.Authenticate(username, password);
                
                    client.Send(message);
                    client.Disconnect(true);
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, e.Message);
                return false;
            }
            
            return true;
        }

        private void ConvertToInternetAddressList(InternetAddressList internetAddressList, IEnumerable<MailAddress> listOfMailAddresses)
        {
            if (listOfMailAddresses is null)
                return;
            
            foreach (var mailAddress in listOfMailAddresses)
            {
                var mailboxAddress = new MailboxAddress(mailAddress.Name, mailAddress.Address);
                internetAddressList.Add(mailboxAddress);
            }
        }
    }
}