﻿using ProSkive.Services.Notifications.Models;

namespace ProSkive.Services.Notifications.Services
{
    public interface IMailService
    {
        bool SendMail(Mail mail);
    }
}