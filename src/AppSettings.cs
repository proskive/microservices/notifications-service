﻿using ProSkive.Lib.Common.Settings;
using ProSkive.Lib.MongoDb.Settings;

namespace ProSkive.Services.Notifications
{
    public class AppSettings
    {
        public JwtSettings Jwt { get; set; }
        public MongoDbSettings MongoDb { get; set; }
        public SmtpSettings Smtp { get; set; }
        public InfoSettings Info { get; set; }
        public LoggingSettings Logging { get; set; }
    }

    public class SmtpSettings
    {
        public UsernamePasswordAuth Authentication { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
    }

    public class UsernamePasswordAuth
    {
        public bool Enabled { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class LoggingSettings
    {
        public bool IncludeScopes { get; set; }
        public LogEntity Debug { get; set; }
        public LogEntity Console { get; set; }
    }

    public class LogEntity
    {
        public LogLevel LogLevel { get; set; }
    }

    public class LogLevel
    {
        public string Default { get; set; }
        public string System { get; set; }
        public string ProSkive { get; set; }
    }
}