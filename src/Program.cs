﻿using ProSkive.Lib.Common.Services.ServiceHost;

namespace ProSkive.Services.Notifications
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ProSkiveServiceHost.Create<Startup>(args)
                .Build()
                .Run();
        }
    }
}