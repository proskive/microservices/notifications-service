﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProSkive.Lib.Api.Resources.Errors;
using ProSkive.Services.Notifications.Models;
using ProSkive.Services.Notifications.Resources.V1;
using ProSkive.Services.Notifications.Services;

namespace ProSkive.Services.Notifications.Controllers.V1
{
    [Authorize]
    [ApiController]
    [Route("api/v1/mail")]
    public class MailController : ControllerBase
    {
        private readonly ILogger<MailController> logger;
        private readonly IMailService mailService;
        private readonly IMapper mapper;

        public MailController(ILogger<MailController> logger, IMailService mailService, IMapper mapper)
        {
            this.logger = logger;
            this.mailService = mailService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Sends an email via SMTP to the recipient.
        /// </summary>
        [HttpPost("send")]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [ProducesResponseType(202)]
        public ActionResult SendMail(SendMailResource sendMailResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(new BadRequestApiResource(ModelState));

            var mail = mapper.Map<SendMailResource, Mail>(sendMailResource);
            if (!mailService.SendMail(mail))
                return StatusCode(StatusCodes.Status500InternalServerError, new InternalServerErrorApiResource("Could not process mail"));

            return Accepted();
        }
    }
}