﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProSkive.Lib.Api.Resources.Errors;
using ProSkive.Services.Notifications.Models;
using ProSkive.Services.Notifications.Repositories;
using ProSkive.Services.Notifications.Resources.V1;

namespace ProSkive.Services.Notifications.Controllers.V1
{
    [Authorize]
    [ApiController]
    [Route("api/v1/notifications")]
    public class NotificationsController : ControllerBase
    {
        private readonly ILogger<NotificationsController> logger;
        private readonly INotificationRepository repository;
        private readonly IMapper mapper;

        public NotificationsController(ILogger<NotificationsController> logger, INotificationRepository repository, IMapper mapper)
        {
            this.logger = logger;
            this.repository = repository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Retrieves all notifications. Can be filtered by the dismissed value.
        /// </summary>
        [HttpGet("")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<ViewNotificationResource>>> Index(bool? dismissed = null)
        {
            var notifications = await repository.GetNotificationsAsync(dismissed);
            return MapToViewResourceList(notifications);
        }

        /// <summary>
        /// Retrieves an explicit notification by id. Returns 404 if not found.
        /// </summary>
        [HttpGet("{id}", Name = RouteNames.NotificationDetail)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<ViewNotificationResource>> FindById(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return BadRequest(new BadRequestApiResource("Please provide an id"));   

            var notification = await repository.FindNotificationByIdAsync(id);
            if (notification is null)
            {
                logger.LogWarning($"Resource not found (FindById({id})");
                return NotFound(new ResourceNotFoundApiResource());
            }
            
            return MapToViewResource(notification);
        }
        
        /// <summary>
        /// Retrieves all notifications by a given tag. Can be filtered by dismissed value.
        /// </summary>
        [HttpGet("tag/{tag}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<ViewNotificationResource>>> FindByTag(string tag, bool? dismissed = null)
        {
            if (string.IsNullOrWhiteSpace(tag))
                return BadRequest(new BadRequestApiResource("Please provide a tag"));

            var notifications = await repository.FindNotificationsByTagAsync(tag, dismissed);
            return MapToViewResourceList(notifications);
        }

        /// <summary>
        /// Retrieves all notifications by listener. Can be filtered by dismissed value.
        /// </summary>
        [HttpGet("listener/{listener}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<ViewNotificationResource>>> FindByListener(string listener, bool? dismissed = null)
        {
            if (string.IsNullOrWhiteSpace(listener))
                return BadRequest(new BadRequestApiResource("Please provide a listener"));

            var notifications = await repository.FindNotificationsByListenerAsync(listener, dismissed);
            return MapToViewResourceList(notifications);
        }
        
        /// <summary>
        /// Retrieves all notifications by tag and listener. Can be filtered by dismissed value.
        /// </summary>
        [HttpGet("tag/{tag}/listener/{listener}")]
        [HttpGet("listener/{listener}/tag/{tag}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<ViewNotificationResource>>> FindByTagAndListener(string tag, string listener, bool? dismissed = null)
        {
            if (string.IsNullOrWhiteSpace(tag) || string.IsNullOrWhiteSpace(listener))
                return BadRequest(new BadRequestApiResource("Please provide a tag and a listener"));


            var notifications = await repository.FindNotificationsByTagAndListenerAsync(tag, listener, dismissed);
            return MapToViewResourceList(notifications);
        }

        /// <summary>
        /// Creates a new notification.
        /// </summary>
        [HttpPost("")]
        [ProducesResponseType(400)]
        [ProducesResponseType(201)]
        public async Task<ActionResult<ViewNotificationResource>> Create(CreateNotificationResource notificationResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(new BadRequestApiResource(ModelState));
            
            var notification = mapper.Map<CreateNotificationResource, Notification>(notificationResource);
            notification.Dismissed = false;
            notification.Date = DateTime.Now;

            var createdNotification = await repository.SaveNotificationAsync(notification);
            var createdNotificationResource = MapToViewResource(createdNotification);
            return CreatedAtRoute(RouteNames.NotificationDetail, new { id = createdNotificationResource.Id }, createdNotificationResource);
        }

        /// <summary>
        /// Creates a new notification for multiple listeners.
        /// </summary>
        [HttpPost("multiple")]
        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<List<ViewNotificationResource>>> CreateMultiple(CreateMultipleNotificationsResource createMultipleNotificationsResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(new BadRequestApiResource(ModelState));

            var savedNotifications = new List<ViewNotificationResource>();
            foreach (var listener in createMultipleNotificationsResource.Listeners)
            {
                var notification = new Notification()
                {
                    Listener = listener,
                    Message = createMultipleNotificationsResource.Message,
                    Link = createMultipleNotificationsResource.Link,
                    Tag = createMultipleNotificationsResource.Tag,
                    Dismissed = false,
                    Date = DateTime.Now,
                };

                var createdNotification = await repository.SaveNotificationAsync(notification);
                savedNotifications.Add(MapToViewResource(createdNotification));
            }
            
            return savedNotifications;
        }

        /// <summary>
        /// Sets the dismissed value of a specific notification to 'true'. Returns 404 if not found.
        /// </summary>
        [HttpPatch("{id}/dismiss")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<ViewNotificationResource>> Dismiss(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return BadRequest(new BadRequestApiResource("Please provide an id"));

            var foundNotification = await repository.FindNotificationByIdAsync(id);
            if (foundNotification is null)
            {
                logger.LogWarning($"Resource not found (Dismiss({id})");
                return NotFound(new ResourceNotFoundApiResource());
            }

            var updatedNotification = await repository.DismissNotificationAsync(foundNotification);
            return MapToViewResource(updatedNotification);
        }
        
        /// <summary>
        /// Sets the dismissed value of a specific notification to 'false'. Returns 404 if not found.
        /// </summary>
        [HttpPatch("{id}/dismiss/undo")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<ViewNotificationResource>> UndoDismiss(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return BadRequest(new BadRequestApiResource("Please provide an id"));
            
            var foundNotification = await repository.FindNotificationByIdAsync(id);
            if (foundNotification is null)
            {
                logger.LogWarning($"Resource not found (UndoDismiss({id})");
                return NotFound(new ResourceNotFoundApiResource());
            }      

            var updatedNotification = await repository.DismissNotificationAsync(foundNotification, undo: true);
            return MapToViewResource(updatedNotification);
        }

        /// <summary>
        /// Deletes a specific notification. Returns 404 if not found.
        /// </summary>
        [HttpDelete("{id}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(204)]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return BadRequest(new BadRequestApiResource("Please provide an id"));
            
            if (!await repository.DeleteNotification(id))
            {
                logger.LogWarning($"Resource not found (Delete({id})");
                return NotFound(new ResourceNotFoundApiResource());
            }
            
            return NoContent();
        }
        
        private ViewNotificationResource MapToViewResource(Notification notification)
        {
            return mapper.Map<Notification, ViewNotificationResource>(notification);
        }
        
        private List<ViewNotificationResource> MapToViewResourceList(List<Notification> notifications)
        {
            return mapper.Map<List<Notification>, List<ViewNotificationResource>>(notifications);
        }
    }
}