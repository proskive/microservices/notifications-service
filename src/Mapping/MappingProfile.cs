﻿using AutoMapper;
using ProSkive.Services.Notifications.Models;
using ProSkive.Services.Notifications.Resources.V1;

namespace ProSkive.Services.Notifications.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Notifications
            CreateMap<Notification, ViewNotificationResource>()
                .ForMember(vnr => vnr.Id, opt => opt.MapFrom(n => n.Id.ToString()));
            CreateMap<CreateNotificationResource, Notification>();
            
            // Mails
            CreateMap<SendMailResource, Mail>();
            CreateMap<MailAddressResource, MailAddress>();
        }
    }
}