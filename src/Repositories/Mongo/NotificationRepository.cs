﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using ProSkive.Lib.MongoDb.Exceptions;
using ProSkive.Lib.MongoDb.Settings.Snapshots;
using ProSkive.Services.Notifications.Models;
using MongoUtils = ProSkive.Lib.MongoDb.MongoUtils;

namespace ProSkive.Services.Notifications.Repositories.Mongo
{
    public class NotificationRepository : INotificationRepository
    {
        private readonly ILogger<NotificationRepository> logger;
        private readonly MongoOptionsSnapshot settings;
        
        public IMongoClient Client { get; }
        public IMongoDatabase Database { get; }
        public IMongoCollection<Notification> Collection { get; }
        
        public NotificationRepository(ILogger<NotificationRepository> logger, IOptionsSnapshot<MongoOptionsSnapshot> settings)
        {
            this.logger = logger;
            this.settings = settings.Value;
            
            var connectionEstablished = MongoUtils.ConnectToCollection<Notification>(
                this.settings.MongoDb,
                out var collection,
                out var client,
                out var database
            );

            if (!connectionEstablished)
                logger.LogError("Could not connect to collection or even database");

            Client = client;
            Database = database;
            Collection = collection;
        }
        
        public async Task<List<Notification>> GetNotificationsAsync(bool? dismissed = null)
        {
            if (Collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            if (dismissed.HasValue)
                return await Collection.Find(n => n.Dismissed == dismissed).ToListAsync();
            
            return await Collection.Find(_ => true).ToListAsync();
        }

        public async Task<Notification> FindNotificationByIdAsync(string id)
        {
            if (Collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            if (await MongoUtils.IsCollectionEmptyAsync(Collection))
                return null;
            
            var notifications = await Collection.Find(n => n.Id == new ObjectId(id)).ToListAsync();
            return notifications.FirstOrDefault();
        }

        public async Task<List<Notification>> FindNotificationsByTagAsync(string tag, bool? dismissed = null)
        {
            if (Collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            if (dismissed.HasValue)
                return await Collection.Find(n => n.Tag == tag && n.Dismissed == dismissed).ToListAsync();
            
            return await Collection.Find(n => n.Tag == tag).ToListAsync();
        }

        public async Task<List<Notification>> FindNotificationsByListenerAsync(string listener, bool? dismissed = null)
        {
            if (Collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            if (dismissed.HasValue)
                return await Collection.Find(n => n.Listener == listener && n.Dismissed == dismissed).ToListAsync();
            
            return await Collection.Find(n => n.Listener == listener).ToListAsync();
        }

        public async Task<List<Notification>> FindNotificationsByTagAndListenerAsync(string tag, string listener, bool? dismissed)
        {
            if (Collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            if (dismissed.HasValue)
                return await Collection.Find(n => n.Tag == tag && n.Listener == listener && n.Dismissed == dismissed).ToListAsync();

            return await Collection.Find(n => n.Tag == tag && n.Listener == listener).ToListAsync();
        }

        public async Task<Notification> SaveNotificationAsync(Notification notification)
        {
            if (Collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            await Collection.InsertOneAsync(notification);
            return notification;
        }

        public async Task<Notification> DismissNotificationAsync(Notification notification, bool undo = false)
        {
            if (Collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            var updateDefinition = new UpdateDefinitionBuilder<Notification>()
                .Set(n => n.Dismissed, true ^ undo);

            var result = await Collection.UpdateOneAsync(n => n.Id == notification.Id, updateDefinition);
            if (result.IsAcknowledged)
                notification.Dismissed = true ^ undo;
            
            return notification;
        }

        public async Task<bool> DeleteNotification(string id)
        {
            if (Collection == null)
                throw new MongoCollectionNotFoundException(settings.MongoDb.CollectionName);
            
            var notifications = await Collection.Find(n => n.Id == new ObjectId(id)).ToListAsync();
            var notification = notifications.FirstOrDefault();
            if (notification is null)
                return false;
            
            await Collection.DeleteOneAsync(n => n.Id == new ObjectId(id));
            return true;
        }
    }
}