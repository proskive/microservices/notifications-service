﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProSkive.Services.Notifications.Models;

namespace ProSkive.Services.Notifications.Repositories
{
    public interface INotificationRepository
    {
        Task<List<Notification>> GetNotificationsAsync(bool? dismissed = null);
        Task<Notification> FindNotificationByIdAsync(string id);
        Task<List<Notification>> FindNotificationsByTagAsync(string tag, bool? dismissed = null);
        Task<List<Notification>> FindNotificationsByListenerAsync(string listener, bool? dismissed = null);
        Task<List<Notification>> FindNotificationsByTagAndListenerAsync(string tag, string listener, bool? dismissed = null);
        Task<Notification> SaveNotificationAsync(Notification notification);
        Task<Notification> DismissNotificationAsync(Notification notification, bool undo = false);
        Task<bool> DeleteNotification(string id);
    }
}