﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProSkive.Lib.Utils.Attributes.Validation;

namespace ProSkive.Services.Notifications.Resources.V1
{
    public class CreateMultipleNotificationsResource
    {
        [Required]
        [MinimalElements(1)]
        public List<string> Listeners { get; set; }
        
        [Required]
        public string Message { get; set; }
        
        public string Link { get; set; }
        
        [Required]
        public string Tag { get; set; }
    }
}