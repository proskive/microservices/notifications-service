﻿using System.ComponentModel.DataAnnotations;

namespace ProSkive.Services.Notifications.Resources.V1
{
    public class MailAddressResource
    {
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string Address { get; set; }
    }
}