﻿using System.ComponentModel.DataAnnotations;

namespace ProSkive.Services.Notifications.Resources.V1
{
    public class CreateNotificationResource
    {
        [Required]
        public string Listener { get; set; }
        
        [Required]
        public string Message { get; set; }
        
        public string Link { get; set; }
        
        [Required]
        public string Tag { get; set; }
    }
}