﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ProSkive.Lib.Utils.Attributes.Validation;

namespace ProSkive.Services.Notifications.Resources.V1
{
    public class SendMailResource
    {
        [Required, MinimalElements(1), ValidateNestedObject]
        public List<MailAddressResource> From { get; set; }
        
        [Required, MinimalElements(1), ValidateNestedObject]
        public List<MailAddressResource> To { get; set; }
        
        [ValidateNestedObject]
        public List<MailAddressResource> Cc { get; set; }
        
        [ValidateNestedObject]
        public List<MailAddressResource> Bcc { get; set; }
        
        [Required]
        public string Subject { get; set; }
        
        [Required]
        public string Body { get; set; }
    }
}