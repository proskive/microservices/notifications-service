﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using AutoMapper;
using LoggingAdvanced.Console;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProSkive.Lib.Common.Extensions;
using ProSkive.Lib.MongoDb.Contributors.Health;
using ProSkive.Lib.MongoDb.Settings.Snapshots;
using ProSkive.Services.Notifications.Repositories;
using ProSkive.Services.Notifications.Repositories.Mongo;
using ProSkive.Services.Notifications.Services;
using ProSkive.Services.Notifications.Services.MailKit;
using Steeltoe.Management.Endpoint.Health;
using Swashbuckle.AspNetCore.Swagger;

namespace ProSkive.Services.Notifications
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Configs
            services.Configure<AppSettings>(Configuration);
            services.Configure<MongoOptionsSnapshot>(Configuration);
            services.Configure<ApiBehaviorOptions>(options => { options.SuppressModelStateInvalidFilter = true; });

            // Allow Cors
            services.AddCors();
            
            // Jwt
            services.AddJwt(Configuration);
            
            // Eureka
            services.AddEureka(Configuration);
            
            // Health Contributors
            services.AddScoped<IHealthContributor, MongoHealthContributor>();
            
            // Health and Info Endpoints
            services.AddHealthAndInfoEndpoints(Configuration);
            
            // Dependency Injection
            services.AddAutoMapper();
            services.AddTransient<INotificationRepository, NotificationRepository>();
            services.AddTransient<IMailService, MailService>();
            
            // Lower Case
            services.AddRouting(options => options.LowercaseUrls = true);
            
            // MVC
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            
            // Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "ProSkive Notifications API", Version = "v1" });
                
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>()
                {
                    { "Bearer", new string[] { }}
                });
                
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",                    
                    Type = "apiKey"
                });
                
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsoleAdvanced(Configuration.GetSection("Logging"));
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            // Cors
            app.UseCors(builder => { builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });
            
            // Auth
            app.UseAuthentication();

            // Swagger
            app.UseSwagger();

            // Swagger UI
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ProSkive Noitification API v1");
            });
            
            // MVC Middleware
            app.UseMvc();
            
            // Use Eureka
            app.UseEureka(Configuration);
            
            // Management
            app.UseHealthAndInfoEndpoints();
        }
    }
}